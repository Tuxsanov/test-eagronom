export const ADD_CATEGORY = 'ADD_CATEGORY';
export const NEXT_PAGE = 'NEXT_PAGE';
export function addCategory(id) {
  return { type: ADD_CATEGORY, id }
}
export function nextPage(page) {
  return { type: NEXT_PAGE, page }
}