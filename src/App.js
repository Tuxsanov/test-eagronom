import React, {Component} from 'react';
import './App.css';
import request from 'request';
import LeftSidebar from './components/left-sidebar.component';
import Gallery from './components/gallery';
import { connect } from 'react-redux';
import { nextPage } from './actions/actions';


const URL = {
  image: 'https://api.thecatapi.com/v1/images/search',
  categories: 'https://api.thecatapi.com/v1/categories',
}

class App extends Component {
  constructor(props){
    super(props);

    this.state = {
      categories: [],
      images: []
    }
  }

  componentDidMount() {
    this.getCategories();  
    this.searchImage(this.props);
  }
  
  getCategories(){
    request({
      uri: URL.categories,
      method: 'GET',
    },(err, res, body)=>{
      this.setState({
        categories: JSON.parse(body)
      })
    });
  }

  searchImage({categories, currentPage}){
    request({
      uri: URL.image,
      qs: {
        limit: 10,
        category_ids: categories,
        page: currentPage
      },
      method: 'GET',
    },(err, res, body)=>{
      this.setState({
        images: currentPage===0?JSON.parse(body):[...this.state.images, ...JSON.parse(body)]
      })
    });
  }

  componentWillReceiveProps(nextParams){
    if(this.props.currentPage === nextParams.currentPage&&nextParams.currentPage>0){
      this.props.dispatch(nextPage(0));
      this.setState({images: []});
    }else{
      this.searchImage(nextParams);
    }
  }

  render(){
    return (
      <div className='container'>
        <LeftSidebar categories={this.state.categories} selectedCategories={this.props.categories} />
        <Gallery images={this.state.images} />
      </div>
    )
  }
}

const mapStateToProps = ({categories, currentPage}) => ({
  categories,
  currentPage
})

export default connect(mapStateToProps)(App)