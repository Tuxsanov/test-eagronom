import { combineReducers } from 'redux'
import categories from './categories'
import currentPage from './page';

export default combineReducers({
  categories,
  currentPage,
})