import { NEXT_PAGE } from "../actions/actions";

const currentPage = (state = 0, action) => {
    switch (action.type) {
        case NEXT_PAGE: return action.page!==0?state += 1:0
        default: return state;
    }
}

export default currentPage