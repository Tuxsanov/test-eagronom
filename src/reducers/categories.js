import { ADD_CATEGORY } from "../actions/actions";

const categories = (state = [], action) => {
    switch (action.type) {
        case ADD_CATEGORY:
            if(state.indexOf(action.id)>-1){
                return state.filter(_e=>_e!==action.id)      
            }else{
                return [
                    ...state,
                    action.id
                ]
            }

        default: return state
    }
}

export default categories