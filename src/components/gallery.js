import React, { Component } from 'react';
import { connect } from 'react-redux';
import { nextPage } from '../actions/actions';

class Gallery extends Component {
    render() {
        const {images = [], dispatch} = this.props;
        return (
            <div className='gallery'>
                {
                    images.map(({
                        url,
                        id
                    },index)=>(
                        <div key={id} className={index%10===0?'fullImg':null} style={{backgroundImage: `url(${url})`}}>
                        </div>
                    ))
                }

                {images.length>0&&<div className='loadMore'>
                    <div  onClick={()=>dispatch(nextPage())}>
                        Load more...
                    </div>
                </div>}
            </div>
        );
    }
}
export default connect()(Gallery)
