import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addCategory } from '../actions/actions';

class LeftSidebar extends Component {
    activeClass(id){
        return this.props.selectedCategories.indexOf(id)>-1;
    }
    render() {
        const {categories = [], dispatch} = this.props;
        return (
            <div className='leftSidebar'>
                <ul>
                    {
                        categories.map(({name, id})=>(
                        <li key={id} className={this.activeClass(id)?'active':null} onClick={()=>dispatch(addCategory(id))} >
                            {name}
                        </li>
                        ))
                    }
                </ul>
            </div>
        )
    }
}

export default connect()(LeftSidebar)