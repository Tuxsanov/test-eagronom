import { ADD_CATEGORY, addCategory, nextPage, NEXT_PAGE } from "./actions/actions";

describe('actions', () => {
    it('should add or remove categories id from the list', () => {
        const id = 0;
        const expectedAction = {
            type: ADD_CATEGORY,
            id
        }
        expect(addCategory(id)).toEqual(expectedAction)
    })
})

describe('actions', () => {
    it('should go to the next page', () => {
        const page = 0;
        const expectedAction = {
            type: NEXT_PAGE,
            page
        }
        expect(nextPage(page)).toEqual(expectedAction)
    })
})